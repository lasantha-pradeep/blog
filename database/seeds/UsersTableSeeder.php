<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user=App\User::create([
            'name'=>'Admin',
            'email'=>'lasantha.pradeep@serverclub.lk',
            'password'=>bcrypt('123456'),
            'admin'=>1
        ]);
        App\Profile::create([
            'user_id'=>$user->id,
            'avatar'=>'uploads/avatars/userimg.png',
            'about'=>'Default Admin User',
            'facebook'=>'facebook.com',
            'youtube'=>'youtube.com'
        ]);
    }
}
