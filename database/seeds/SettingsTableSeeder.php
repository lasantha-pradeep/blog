<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Setting::create([
            'site_name'=>'Practice Blog',
            'address'=>'Badulla,Sri Lanka',
            'contact_number'=>'+947xxxxxxxxx',
            'contact_email'=>'your@email.com'
        ]);
    }
}
